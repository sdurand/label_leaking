# label_leaking

Code to reproduce extreme label leaking phenomenon.

Label leaking in the adversarial training context has been discussed in [Adversarial Machine Learning at Scale](https://arxiv.org/abs/1611.01236),
and on this [blog post](http://jackhaha363.github.io/post/label_leak/).

We show here extreme cases of label leaking.

## Main takeaway

Do not use attacks that use ground-truth labels (PGD, FGSM...) on secondary models (transferred attacks) to 
train your models: the labels will leak, your model will learn how to decode the attack and recover the 
true label.
## Installation
You will need pytorch, torchvision, pytorch-lightning (1.6, NOT 1.7, [adversarial training does not mix well with 
the new torch.inference_mode](https://github.com/Lightning-AI/lightning/discussions/14782)).

We used a copied version of torchattacks, waiting for the release with latest changes as we need the 
possibility to change the device of a created attack to handle multi-gpu training.

We also use timm for the model zoo.

We suggest using conda or better yet [mamba](https://github.com/mamba-org/mamba):
``` bash
mamba create -n label_leaking python=3.10
conda activate label_leaking
mamba install -c pytorch pytorch=1.12 torchvision=0.13 cudatoolkit=11.3
mamba install -c conda-forge timm pytorch-lightning=1.6 matplotlib tensorboard numpy scipy pandas
pip install torchattacks
```
Replace mamba with conda if you want to stick to conda.
Remove cudatoolkit=11.3 if you don't have gpu.
A requirements.txt is also provided and should be sufficient to install everything needed with pip (no guarantees though).
## Getting started
Example script for clean training and evaluation on PGD attack:

```python
import torchattacks

from backbones import FNN
from data_utils import get_loaders
from trainer import train_adv
# loaders is a 3-uple (train_loader, val_loader, test_loader)
loaders = get_loaders("mnist", batch_size=100)

# simple fully connected network, check backbones
model = FNN(
    input_shape=(28, 28),
    layer_sizes=(256, 256, 256, 256, 10),
    activation="relu"
)
# we put the model on cuda since we're using gpus.
# it's not clear if it works on multi-gpus.
val_atk = torchattacks.PGD(model.cuda(), eps=0.2, steps=20),
# Vanilla training with PGD attack only for the validation / test
trainer, pl_model = train_adv(
    model=model,
    loaders=loaders,
    name="mnist",
    val_atk=val_atk,
    sub_dir="vanilla_FNN",
    clean=True,
    early_stopping=False,
    weights=(0, 1),
    max_epochs=8,
)

# training
trainer.fit(pl_model)
# testing
trainer.test(pl_model)
```

Example script illustrating label leaking:
train a simple model A on attacks generated from a (non-trained) model B.
Both simple FNN

```python
model_A = FNN(
    input_shape=(28, 28),
    layer_sizes=(256, 256, 256, 256, 10),
    activation="relu"
)

model_B = FNN(
    input_shape=(28, 28),
    layer_sizes=(256, 256, 256, 256, 10),
    activation="relu"
)
atk = torchattacks.PGD(model_B.cuda(), eps=0.2, steps=20)
# Vanilla training with PGD attack only for the validation / test
trainer, pl_model = train_adv(
    model=model_A,
    loaders=loaders,
    name="mnist",
    attack = atk,
    sub_dir="FNN_random_label_leaking",
    clean=False,
    early_stopping=False,
    weights=(0, 1),
    max_epochs=8,
)

# training
trainer.fit(pl_model)
# testing
trainer.test(pl_model)
# testing on attack on itself:
pl_model.val_atk = torchattacks.PGD(pl_model.model.cuda(), eps=0.2, steps=20)
trainer.test(pl_model)
```

Results (orange = model trained on adversarial samples from untrained model, 
purple = model trained on clean images)
Accuracy during training (trainset) (for the orange, it's accuracy on adversarial samples from model B):

![](imgs/fnn_fnn/train.png)

Label leaking detected: the model has near perfect accuracy as early as epoch 1 (the curve is smoothed)

Accuracy on clean samples (validation set):

![](imgs/fnn_fnn/clean.png)

The model only trained on adversaries does poorly on clean samples

Accuracy on adversarial samples on trained model (validation set):

![](imgs/fnn_fnn/adv.png)

(be careful of scale...) Both model have near 0% accuracy on adversarial samples.


See examples folder for more usage example.
## Questions to explore

Reminder: Model A is the model being trained, model B is the model being attacked

- [ ] What if Model B is with a different architecture from model A ? With a lot more parameters ?
- [ ] What if Model B is trained ? 
- [ ] What if Model B is adversarially trained ?
- [ ] What about different attacks (FGSM etc) ? What about different epsilons ? Varying epsilons ?
- [ ] Does it depend on optimizer used (SGD vs Adam)?
- [ ] What about dataset? CIFAR10, higher resolution?
- [ ] What about multiattribute classification (celebA)?

From preliminary experiments I expect it always happen to some extent, as long as model B remains unchanged during
 training of model A.
It just takes longer when model A is more simpler than model B, or model B is trained.

## Authors
Serge DURAND
