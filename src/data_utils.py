from __future__ import annotations

import os
from typing import Callable, Optional, Tuple, Any

import PIL
import pandas as pd
import torchvision.transforms as T
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torchvision import transforms
from torchvision.datasets import MNIST, CIFAR10
from torchvision.datasets import VisionDataset
from torchvision.datasets.folder import default_loader
from torchvision.datasets.utils import download_url, download_and_extract_archive, verify_str_arg

from config import PROJ_DIR


def default_transforms(img_size=224):
    """
    Create "standard" composition of data_transforms for CUB like dataset:
    RandomResizeCrop(img_size), RandomHorizontalFlip, ToTensor, Normalize
    For test we resize slightly bigger then center crop to img_size
    Args:
        img_size:

    Returns:
        train_transform, test_transform

    """
    train_transform_list = [
        transforms.RandomResizedCrop(img_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]
    test_transforms_list = [
        transforms.Resize(int(img_size / 0.875)),
        transforms.CenterCrop(img_size),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]

    return train_transform_list, test_transforms_list

def get_loaders(dataset="mnist",
                batch_size=100,
                num_workers=6,
                root=PROJ_DIR.resolve() / "dataset",
                data_transforms=None,
                img_size=None):
    """Generate dataloaders for mnist or cifar.
    Change num_workers to maximum possible for speed efficiency (available cpus - nb of gpus).
    Test batch size will be batch_size * 2

    Args:
      dataset: param batch_size: (Default value = "mnist")
      num_workers: param root: (Default value = 6)
      data_transforms: return: (Default value = None)
      batch_size: (Default value = 100)
      root: (Default value = PROJ_DIR / "dataset")
      img_size (int): resize resolution. For ImageNet, Cub and such, not cifar or mnist.
      (Default value = none)

    Returns:
        a tuple (train loader, validation loader, test loader)

    """
    if dataset.lower() == "mnist":
        data_transforms = data_transforms if data_transforms is not None else T.ToTensor()

        train_set = MNIST(root=root,
                          transform=data_transforms,
                          download=True,
                          train=True
                          )

        test_set = MNIST(root=root,
                         transform=data_transforms,
                         download=True,
                         train=False
                         )
    elif dataset.lower() == "cifar":
        data_transforms = data_transforms if data_transforms is not None else T.ToTensor()

        train_set = CIFAR10(root=root,
                            transform=data_transforms,
                            download=True,
                            train=True
                            )

        test_set = CIFAR10(root=root,
                           transform=data_transforms,
                           download=True,
                           train=False
                           )

    elif "cub" in dataset.lower():
        img_size = img_size if img_size is not None else 224
        train_transform_list, test_transforms_list = default_transforms(img_size)

        train_set = Cub2011(root="~/cub/", train=True,
                            transform=transforms.Compose(train_transform_list))
        test_set = Cub2011(root="~/cub/", train=False,
                           transform=transforms.Compose(test_transforms_list))

    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=num_workers)
    test_loader = DataLoader(test_set, batch_size=batch_size*2, shuffle=False, num_workers=num_workers)
    val_loader = DataLoader(test_set, batch_size=batch_size*2, shuffle=False, num_workers=num_workers)

    return train_loader, val_loader, test_loader



class Cub2011(Dataset):
    base_folder = 'CUB_200_2011/images'
    url = 'http://www.vision.caltech.edu/visipedia-data/CUB-200-2011/CUB_200_2011.tgz'
    filename = 'CUB_200_2011.tgz'
    tgz_md5 = '97eceeb196236b17998738112f37df78'

    def __init__(self, root, train=True, transform=None, loader=default_loader, download=True):
        self.root = os.path.expanduser(root)
        self.transform = transform
        self.loader = loader
        self.train = train

        if download:
            self._download()

        if not self._check_integrity():
            raise RuntimeError('Dataset not found or corrupted.' +
                               ' You can use download=True to download it')

    def _load_metadata(self):
        images = pd.read_csv(os.path.join(self.root, 'CUB_200_2011', 'images.txt'), sep=' ',
                             names=['img_id', 'filepath'])
        image_class_labels = pd.read_csv(os.path.join(self.root, 'CUB_200_2011', 'image_class_labels.txt'),
                                         sep=' ', names=['img_id', 'target'])
        train_test_split = pd.read_csv(os.path.join(self.root, 'CUB_200_2011', 'train_test_split.txt'),
                                       sep=' ', names=['img_id', 'is_training_img'])

        data = images.merge(image_class_labels, on='img_id')
        self.data = data.merge(train_test_split, on='img_id')

        if self.train:
            self.data = self.data[self.data.is_training_img == 1]
        else:
            self.data = self.data[self.data.is_training_img == 0]

    def _check_integrity(self):
        try:
            self._load_metadata()
        except Exception:
            return False

        for index, row in self.data.iterrows():
            filepath = os.path.join(self.root, self.base_folder, row.filepath)
            if not os.path.isfile(filepath):
                print(filepath)
                return False
        return True

    def _download(self):
        import tarfile

        if self._check_integrity():
            print('Files already downloaded and verified')
            return

        download_url(self.url, self.root, self.filename, self.tgz_md5)

        with tarfile.open(os.path.join(self.root, self.filename), "r:gz") as tar:
            tar.extractall(path=self.root)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        sample = self.data.iloc[idx]
        path = os.path.join(self.root, self.base_folder, sample.filepath)
        target = sample.target - 1  # Targets start at 1 by default, so shift to 0
        img = self.loader(path)

        if self.transform is not None:
            img = self.transform(img)

        return img, target


class FGVCAircraft(VisionDataset):
    """`FGVC Aircraft <https://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/>`_ Dataset.

    The dataset contains 10,200 images of aircraft, with 100 images for each of 102
    different aircraft model variants, most of which are airplanes.
    Aircraft models are organized in a three-levels hierarchy. The three levels, from
    finer to coarser, are:

    - ``variant``, e.g. Boeing 737-700. A variant collapses all the models that are visually
        indistinguishable into one class. The dataset comprises 102 different variants.
    - ``family``, e.g. Boeing 737. The dataset comprises 70 different families.
    - ``manufacturer``, e.g. Boeing. The dataset comprises 41 different manufacturers.

    Args:
        root (string): Root directory of the FGVC Aircraft dataset.
        split (string, optional): The dataset split, supports ``train``, ``val``,
            ``trainval`` and ``test``.
        annotation_level (str, optional): The annotation level, supports ``variant``,
            ``family`` and ``manufacturer``.
        transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``data_transforms.RandomCrop``
        target_transform (callable, optional): A function/transform that takes in the
            target and data_transforms it.
        download (bool, optional): If True, downloads the dataset from the internet and
            puts it in root directory. If dataset is already downloaded, it is not
            downloaded again.
    """

    _URL = "https://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/archives/fgvc-aircraft-2013b.tar.gz"

    def __init__(
            self,
            root: str,
            split: str = "trainval",
            annotation_level: str = "variant",
            transform: Optional[Callable] = None,
            target_transform: Optional[Callable] = None,
            download: bool = False,
    ) -> None:
        super().__init__(root, transform=transform, target_transform=target_transform)
        self._split = verify_str_arg(split, "split", ("train", "val", "trainval", "test"))
        self._annotation_level = verify_str_arg(
            annotation_level, "annotation_level", ("variant", "family", "manufacturer")
        )

        self._data_path = os.path.join(self.root, "fgvc-aircraft-2013b")
        if download:
            self._download()

        if not self._check_exists():
            raise RuntimeError("Dataset not found. You can use download=True to download it")

        annotation_file = os.path.join(
            self._data_path,
            "data",
            {
                "variant": "variants.txt",
                "family": "families.txt",
                "manufacturer": "manufacturers.txt",
            }[self._annotation_level],
        )
        with open(annotation_file, "r") as f:
            self.classes = [line.strip() for line in f]

        self.class_to_idx = dict(zip(self.classes, range(len(self.classes))))

        image_data_folder = os.path.join(self._data_path, "data", "images")
        labels_file = os.path.join(self._data_path, "data", f"images_{self._annotation_level}_{self._split}.txt")

        self._image_files = []
        self._labels = []

        with open(labels_file, "r") as f:
            for line in f:
                image_name, label_name = line.strip().split(" ", 1)
                self._image_files.append(os.path.join(image_data_folder, f"{image_name}.jpg"))
                self._labels.append(self.class_to_idx[label_name])

    def __len__(self) -> int:
        return len(self._image_files)

    def __getitem__(self, idx) -> Tuple[Any, Any]:
        image_file, label = self._image_files[idx], self._labels[idx]
        image = PIL.Image.open(image_file).convert("RGB")

        if self.transform:
            image = self.transform(image)

        if self.target_transform:
            label = self.target_transform(label)

        return image, label

    def _download(self) -> None:
        """
        Download the FGVC Aircraft dataset archive and extract it under root.
        """
        if self._check_exists():
            return
        download_and_extract_archive(self._URL, self.root)

    def _check_exists(self) -> bool:
        return os.path.exists(self._data_path) and os.path.isdir(self._data_path)


if __name__ == "__main__":

    loaders = get_loaders("cub", batch_size=64, num_workers=5)
