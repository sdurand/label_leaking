import copy

import pytorch_lightning as pl
import torch
from pytorch_lightning import Callback
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from pytorch_lightning.loggers import CSVLogger, TensorBoardLogger
from torch import nn

from advModel import AdvModel
from config import PROJ_DIR


# Callback to save models.
class SaveModel(Callback):
    """ """

    def __init__(self, name):
        super().__init__()
        self.name = name

    def on_epoch_end(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        """

        Args:
          trainer: 'pl.Trainer':
          pl_module: 'pl.LightningModule':
          trainer: 'pl.Trainer': 
          pl_module: 'pl.LightningModule': 

        Returns:

        """
        torch.save(copy.deepcopy(pl_module.model).cpu(),
                   PROJ_DIR / "{}_{}.pth".format(self.name, pl_module.current_epoch))


def train_adv(model: nn.Module,
              name,
              attack=None,
              max_epochs=15,
              monitor="clean_val_loss",
              batch_size=32,
              freeze=False,
              resume_checkpoint=None,
              accumulate_grad_batches=4,
              save_intermediate=False,
              val_atk=None,
              max_test=None,
              reset=5,
              lr=None,
              early_stopping=True,
              es_mode="min",
              top_k=3,
              gpus=1,
              loaders=None,
              weights=(0.5, 0.5),
              clean=False,
              sub_dir=None,
              version=None,
              optim="AdamW",
              fast_schedule=None,
              atk_model=None,
              prune_callback=None,
              attack_kwargs=None):
    """Check AdvModel docstring for most of parameters,
    and pytorch-lightning documentation for more details.

    Args:
        model: nn.Module: the base model to train
        name: results and models will be saved in a folder results/name
        max_epochs: number of epochs if no early stopping. (Default value = 15)
        monitor: value to monitor for early stopping.
            E.g 'clean_val_loss', 'adv_val_loss' etc. (Default value = "clean_val_loss")
        resume_checkpoint: Checkpoint to use to continue training. (Default value = None)
        accumulate_grad_batches: If memory is not enough reduce batch size and set a
            accumulate_grad_batches value to > 1. Ex: batch_size 16 and acc_grad_batch = 4 will
            simulate batch_size = 64. (Default value = 4)
        save_intermediate: saving model at each epoch end (Default value = False)
        early_stopping: use early stopping (Default value = True)
        es_mode: max' or 'min'. Should use 'min' with loss criteria,
            max with accuracy criteria (Default value = "min")
        gpus: number of gpus (Default value = 1)
        sub_dir: create sub dir to log result (results/name/subdir) (Default value = None)
        version: add a version number for loggers. (Default value = None)
        attack: (Default value = None)
        batch_size: (Default value = 32)
        freeze: (Default value = False)
        val_atk: (Default value = None)
        max_test: (Default value = None)
        reset: (Default value = 5)
        lr: (Default value = None)
        loaders: (Default value = None)
        weights: (Default value = (0.5, 0.5):
        clean: (Default value = False)
        optim: (Default value = "AdamW")
        fast_schedule: (Default value = None)
    Returns:
        trainer: a pytorch lightning trainer
        pl_model: a pytorch lightning model

        To be used with trainer.fit(pl_model) for instance.

    """
    save_dir = PROJ_DIR / "results"
    save_dir = save_dir.resolve()
    _dir = save_dir / name
    if not _dir.is_dir():
        _dir.mkdir()
    logger1 = CSVLogger(save_dir=save_dir, name=name, version=version)
    logger2 = TensorBoardLogger(save_dir=save_dir, name=name, sub_dir=sub_dir, version=version)
    callbacks = [pl.callbacks.progress.TQDMProgressBar(refresh_rate=1)]
    if early_stopping:
        callbacks += [EarlyStopping(monitor=monitor, patience=15, min_delta=1e-5, mode=es_mode)]
    if save_intermediate:
        callbacks.append(SaveModel(name))
    if prune_callback is not None:
        callbacks.append(prune_callback)

    checkpoint_callback = ModelCheckpoint(
        monitor=monitor,
        dirpath=save_dir / "models",
        filename=f"{sub_dir}-{{epoch:02d}}-{{{monitor}:.4f}}",
        save_top_k=top_k,
        mode=es_mode,
        save_weights_only=True
    )
    callbacks.append(checkpoint_callback)
    accelerator = "gpu" if gpus >= 1 else "cpu"
    strategy = "dp" if gpus >= 1 else None
    trainer = pl.Trainer(devices=gpus if gpus > 0 else None,
                         accelerator=accelerator,
                         strategy=strategy,
                         max_epochs=max_epochs,
                         val_check_interval=1.0,
                         default_root_dir=PROJ_DIR / f"results/{name}", logger=[logger1, logger2],
                         callbacks=callbacks,
                         accumulate_grad_batches=accumulate_grad_batches,
                         resume_from_checkpoint=resume_checkpoint,
                         )
    pl_model = AdvModel(model,
                        loaders=loaders,
                        attack=attack,
                        batch_size=batch_size,
                        freeze=freeze,
                        val_atk=val_atk,
                        max_test=max_test,
                        lr=lr,
                        weights=weights,
                        clean=clean,
                        optim=optim,
                        fast_schedule=fast_schedule,
                        atk_model=atk_model,
                        attack_kwargs=attack_kwargs)
    print(pl_model.hparams)
    print("Size train = {}".format(len(pl_model.train_dataloader().dataset)))
    print("Size val = {}".format(len(pl_model.val_dataloader().dataset)))
    print("Size test = {}".format(len(pl_model.test_dataloader().dataset)))

    return trainer, pl_model
