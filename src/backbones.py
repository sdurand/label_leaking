import numpy as np
import torch
from torch import nn

class FNN(nn.Module):
    """Simple FeedForward Neural Network."""

    def __init__(self, input_shape, layer_sizes, activation="relu"):
        """

        Args:
            input_shape (int or tuple(int)): the expected input shape
            layer_sizes (tuple[int]): size of hidden layers
            activation (str): activation used in hidden layers
        """
        super().__init__()
        self.input_shape = input_shape
        self.layer_sizes = layer_sizes
        modules = list()
        cur_size = self.layer_sizes[0]
        if isinstance(self.input_shape, (tuple, list)):
            modules.append(nn.Flatten())
            modules.append(nn.Linear(int(np.prod(np.array(self.input_shape))), cur_size))
        else:
            modules.append(nn.Linear(input_shape, cur_size))
        if "relu" in activation:
            modules.append(nn.ReLU())
        elif "sigmoid" in activation.lower():
            modules.append((nn.Sigmoid()))
        elif "tanh" in activation.lower():
            modules.append(nn.Tanh())

        for i, size in enumerate(layer_sizes[1:]):
            modules.append(nn.Linear(in_features=cur_size, out_features=size))
            cur_size = size
            if i != len(layer_sizes[1:]) - 1:
                if "relu" in activation:
                    modules.append(nn.ReLU())
                elif "sigmoid" in activation.lower():
                    modules.append((nn.Sigmoid()))
                elif "tanh" in activation.lower():
                    modules.append(nn.Tanh())
        self.model = nn.Sequential(*modules)

    def forward(self, x):
        """

        Args:
          x: 

        Returns:

        """
        return self.model(x)


class CNN(nn.Module):
    """Simple Convolutional Network. No avg pool or max pool for the moment."""

    def __init__(self, input_shape, conv_params, classif_params=None, activation="relu", avg_pool=False):
        """

        Args:
            input_shape (int or tuple(int)): the expected input shape
            conv_params (list(int, int, int, int): a list of 4-uples specifying for each convolutional layer:
                        (output_channels, kernel_size, stride, padding).
            classif_params: a list of sizes for the classifier (a fully connected network).
            activation (str): the activation used for both the convolutional layers and the classifier
            avg_pool (bool): set to True to add pooling after the convolutions.
        """
        super().__init__()
        self.input_shape = input_shape
        self.avg_pool = avg_pool
        modules = list()
        # input_shape is assumed to be (H,W,C)
        in_channels = self.input_shape[-1]

        for i, params in enumerate(conv_params):
            out_channels, kernel_size, stride, padding = params
            _layer = nn.Conv2d(in_channels=in_channels,
                               out_channels=out_channels,
                               kernel_size=kernel_size,
                               stride=stride,
                               padding=padding)

            modules.append(_layer)
            if self.avg_pool:
                modules.append(
                    nn.AvgPool2d(kernel_size(2, 2), stride=(2, 2))
                )
            in_channels = out_channels
            if "relu" in activation:
                modules.append(nn.ReLU())
            elif "sigmoid" in activation.lower():
                modules.append((nn.Sigmoid()))
            elif "tanh" in activation.lower():
                modules.append(nn.Tanh())
        if classif_params is not None and len(classif_params) != 0:
            modules.append(nn.Flatten())
            H = self.input_shape[0]
            W = self.input_shape[1]
            C = self.input_shape[2]
            x = torch.rand(1, C, H, W)
            # computing classif shape
            with torch.no_grad():
                cur_size = torch.nn.Sequential(*modules)(x).shape[-1]
            for i, size in enumerate(classif_params):
                modules.append(nn.Linear(in_features=cur_size, out_features=size))
                cur_size = size
                if i != len(classif_params) - 1:
                    if "relu" in activation:
                        modules.append(nn.ReLU())
                    elif "sigmoid" in activation.lower():
                        modules.append((nn.Sigmoid()))
                    elif "tanh" in activation.lower():
                        modules.append(nn.Tanh())
        self.model = nn.Sequential(*modules)

    def forward(self, x):
        """

        Args:
          x: 

        Returns:

        """
        return self.model(x)


if __name__ == "__main__":
    model = CNN(
        input_shape=(28, 28, 1),
        conv_params=[(16, 4, 2, 1), (32, 4, 2, 1)],
        classif_params=[100, 10],
        activation="relu"
    )
