import sys
from pathlib import Path

cur_dir = Path(sys.path[0])
PROJ_DIR = cur_dir.absolute().parent
SRC_DIR = PROJ_DIR / "src"
MODELS_DIR = PROJ_DIR / "models"

if __name__ == "__main__":
    print(PROJ_DIR.absolute())
    print(SRC_DIR.absolute())
    print(Path.cwd())
