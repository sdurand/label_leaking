import random

import pytorch_lightning as pl
import torch
from torch import nn
from torch.utils.data import Subset

import atks_pckg.torchattacks as torchattacks
from atks_pckg.torchattacks.attack import Attack
from backbones import FNN, CNN
from config import PROJ_DIR


class AdvModel(pl.LightningModule):
    """ """

    def __init__(self,
                 model: nn.Module,
                 atk_model=None,
                 val_atk_model=None,
                 attack: Attack = None,
                 attack_kwargs=None,
                 loaders=None,
                 loss_fn=nn.CrossEntropyLoss(),
                 freeze=False,
                 batch_size=32,
                 val_atk: Attack = None,
                 max_test=None,
                 fast_schedule=None,
                 lr=None,
                 weights=None,
                 clean=False,
                 optim="AdamW",
                 ):
        """
        A Lightning Module with possibility of adversarial training.

        To do adversarial training
        Args:
          model: Base model to train (aka backbone)
          attack: Attack: The attack to use for adversarial training.
          It can be a already instantiated attack, or a constructor.
          If it is a constructor keyword arguments can be passed in attack_kwargs.
          For constructor a specific model to use for attack can be passed in atk_model or val_atk_model.
          val_atk: Attack:  (Default value = None)
              Attack to use to evaluate model during validation AND testing.
              By default it will be the same as attack used during training.
              It is possible to leave attack None but to use a val_atk, it
              will use vanilla training but record adversarial accuracy on the
              vanilla model being trained.
          loaders: pytorch loaders (Default value = None)
          atk_model: A model to be used to compute adversarial samples.
          If unused the backbone will be used.
          val_atk_model: A model to be used to compute adversarial samples during validation.
          If unused atk_model will be used, or the backbone by default.
          loss_fn: the loss function (Default value = nn.CrossEntropyLoss())
          freeze: If True only the head/classifier will be trained (Default value = False)
          batch_size: param val_atk: The attack to use during validation (Default value = 32)
          max_test: The maximum size of the test and validation set.
        If it is not None a subset of size max_test will be used for
        both validation and test. (Default value = None)
          fast_schedule: Schedule for learning rate. (Default value = None)
          lr: Starting learning rate (Default value = None)
          weights: Weights to balance between clean sample and adversarial sample
        during training.
        E.g (0.5, 0.5) will mean that there is on average half clean images and half adversarial
        images seen during training.
        (0, 1) means only trained on adversarial samples.
        (1, 0) means clean training. (Default value = None)
          clean: If True training will be only on clean images (Default value = False)
          optim: Optimize to use. Default: AdamW. options: 'SGD', 'Adam', 'AdamW'
        """
        super().__init__()
        self.model = model
        # ATTACK
        self.atk_model = atk_model
        self.val_atk_model = val_atk_model if val_atk_model is not None else atk_model
        self.atk = attack
        self.attack_kwargs = attack_kwargs
        self.val_atk = attack if val_atk is None else val_atk

        # this assumes the classifier is the last layer of the named_children
        self.classifier = [n for n, _ in self.model.named_children()][-1]
        # print("Classifier is {}, type : {}".format(self.classifier,type(list(self.model.named_children())[-1][
        # -1]).__name__))
        self.loss_fn = loss_fn
        self.freeze = freeze
        self.batch_size = batch_size
        self.loaders = loaders
        self.cur_epoch_train_acc = []
        self.cur_epoch_train_loss = []

        if max_test is not None and self.loaders is not None:
            n = len(self.loaders[1].dataset)
            new_idx = random.sample(range(n), k=max_test)
            self.loaders[1].dataset.data = Subset(self.loaders[1].dataset.data, new_idx)
            n = len(self.loaders[2].dataset)
            new_idx = random.sample(range(n), k=max_test)
            self.loaders[2].dataset.data = Subset(self.loaders[2].dataset.data, new_idx)

        self.size = len(self.train_dataloader().dataset) if self.loaders is not None else 0
        self.save_hyperparameters("batch_size", "weights", "freeze", "lr")
        self.fast_schedule = fast_schedule
        self.lr = lr
        self.weights = weights
        self.clean = clean
        if optim is None:
            self.optim = torch.optim.AdamW
        elif optim == "AdamW":
            self.optim = torch.optim.AdamW
        elif optim == "Adam":
            self.optim = torch.optim.Adam
        elif optim == "SGD":
            self.optim = torch.optim.SGD
        else:
            raise ValueError(f"Optim should be in '[AdamW, Adam, SGD]', not {optim}")
        self.save_hyperparameters("batch_size", "weights", "freeze", "lr")

    def _get_attacker(self, val=False):
        """
        Helper function to build the attacker depending on params.
        Building an attack object whenever computing adversarial samples
        is necessary for multigpu training.
        """
        atk = self.atk if not val else self.val_atk
        if atk is None:
            return atk
        if isinstance(atk, Attack):
            return atk
        # we assume now that atk is a attack constructor
        # the model used for attack might be different from backbone:
        if val:
            model = self.val_atk_model if self.val_atk_model is not None else self.model
        else:
            model = self.atk_model if self.atk_model is not None else self.model
        if self.attack_kwargs is not None:
            atk = atk(model, **self.attack_kwargs)
        else:
            atk = atk(model)
        return atk

    def generate_adv(self, imgs, labels, val=False):
        """

        Args:
            imgs:
            labels:

        Returns:

        """
        atk = self._get_attacker(val)
        if atk is None:
            return imgs
        # atk.set_device(imgs.device)
        adv_img = atk(imgs, labels)
        return adv_img

    def forward(self, x, clean=None, adv=False, labels=None):
        """

        Args:
          x: 
          clean: (Default value = None)

        Returns:

        """
        return self.model(x, adv=adv, labels=labels)

    def training_step(self, batch, batch_nb):
        """

        Args:
          batch: 
          batch_nb: 

        Returns:

        """
        imgs, labels = batch
        if not self.clean:
            adv_img = self.generate_adv(imgs, labels, val=False)
            if self.weights is None:
                imgs = adv_img
            else:
                imgs = random.choices([imgs, adv_img], weights=self.weights, k=1)[0]
        logits = self.model(imgs)
        loss = self.loss_fn(logits, labels)
        self.log("train_loss", loss, prog_bar=True, on_step=False, on_epoch=True)
        acc = (logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.cur_epoch_train_loss.append(loss)
        self.cur_epoch_train_acc.append(acc)
        self.log("train_acc", acc, prog_bar=True, on_step=False, on_epoch=True)

        return {"loss": loss, "acc": acc}

    def validation_step(self, batch, batch_idx):
        """

        Args:
          batch: 
          batch_idx: 

        Returns:

        """
        imgs, labels = batch

        clean_logits = self.model(imgs)
        clean_loss = self.loss_fn(clean_logits, labels)
        clean_acc = (clean_logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.log("clean_val_loss", clean_loss, prog_bar=True)
        self.log('clean_val_acc', clean_acc, prog_bar=True)
        if self.val_atk is None:
            return clean_loss, clean_acc
        with torch.enable_grad():
            # print(next(self.model.atk.model.parameters()).device)
            # print(next(self.model.parameters()).device)
            adv_img = self.generate_adv(imgs, labels, val=True)

        adv_logits = self.model(adv_img)
        adv_loss = self.loss_fn(adv_logits, labels)
        self.log("adv_val_loss", adv_loss, prog_bar=True)
        adv_acc = (adv_logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.log('adv_val_acc', adv_acc, prog_bar=True)

        return clean_loss, clean_acc, adv_loss, adv_acc

    def test_step(self, batch, batch_idx):
        """

        Args:
          batch: 
          batch_idx: 

        Returns:

        """
        imgs, labels = batch

        clean_logits = self.model(imgs)
        clean_loss = self.loss_fn(clean_logits, labels)
        clean_acc = (clean_logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)

        self.log("clean_test_loss", clean_loss, prog_bar=True)
        self.log('clean_test_acc', clean_acc, prog_bar=True)
        if self.val_atk is None:
            return clean_loss, clean_acc
        with torch.enable_grad():
            adv_img = self.generate_adv(imgs, labels, val=True)
        adv_logits = self.model(adv_img)
        adv_loss = self.loss_fn(adv_logits, labels)
        self.log("adv_test_loss", adv_loss, prog_bar=True)
        adv_acc = (adv_logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.log('adv_test_acc', adv_acc, prog_bar=True)

        return clean_loss, clean_acc, adv_loss, adv_acc

    def configure_optimizers(self):
        """ """

        def build_optim(optim, params):
            """Args:
              optim:
            
            Args:
              optim:

            Args:
              optim: 
              params: 

            Returns:
              

            """
            if issubclass(optim, torch.optim.SGD):
                if self.lr is not None:
                    return optim(params, lr=self.lr, momentum=0.9, weight_decay=1e-4)
                else:
                    return optim(params, momentum=0.9, weight_decay=1e-4)
            elif issubclass(optim, (torch.optim.Adam, torch.optim.AdamW)):
                if self.lr is not None:
                    return optim(params, lr=self.lr, weight_decay=1e-4)
                else:
                    return optim(params, weight_decay=1e-4)

        if self.freeze:
            for name, params in self.model.named_modules():
                if name in ["fc", "classif", "head", "classifier", "last_linear", "conv_head"]:
                    for _params in params.parameters():
                        _params.requires_grad = True
                    continue
                for _name, trained_params in params.named_parameters():
                    trained_params.requires_grad = False
            print("Only optimizing classifier")
            if self.classifier == "fc":
                optim = build_optim(self.optim, self.model.fc.parameters())
            elif self.classifier == "head":
                optim = build_optim(self.optim, self.model.head.parameters())
            elif self.classifier == "classifier":
                optim = build_optim(self.optim, self.model.classifier.parameters())
            else:
                raise ValueError(f"can't freeze for {self.classifier}")
        else:
            print("Optimizing everything")
            optim = build_optim(self.optim, self.model.parameters())
        if self.fast_schedule is not None:
            if isinstance(self.fast_schedule, dict):
                milestones = self.fast_schedule["milestones"]
                gamma = self.fast_schedule["gamma"]
                scheduler = torch.optim.lr_scheduler.MultiStepLR(optim, milestones=milestones, gamma=gamma)
            else:
                scheduler = torch.optim.lr_scheduler.LambdaLR(optim, self.fast_schedule)
            return {"optimizer": optim, "lr_scheduler": scheduler}
        return optim

    def train_dataloader(self):
        """ """
        return self.loaders[0]

    def val_dataloader(self):
        """ """

        return self.loaders[1]

    def test_dataloader(self):
        """ """
        return self.loaders[2]


def load_model(
        checkpoint,
        model: pl.LightningModule = FNN(
            input_shape=(32, 32, 3),
            layer_sizes=(256, 256, 256, 256, 10),
            activation="relu"
        )):
    advmodel = AdvModel(model=model)
    loaded_model = advmodel.load_from_checkpoint(checkpoint, model=model)
    model = loaded_model.model
    return model


if __name__ == "__main__":
    backbone = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    model = CNN(
        input_shape=(32, 32, 3),
        conv_params=[(16, 4, 2, 1), (32, 4, 2, 1)],
        classif_params=[512, 10],
        activation="relu"
    )

    model = load_model(PROJ_DIR / "results/models/pruned_CNN_1_0.95-epoch=45-clean_val_acc=0.6411.ckpt",
                       model)

    torch.save(model.cpu(), PROJ_DIR / "cifar_models/pruned_directly.pth")
    torch.onnx.export(model.cpu(),
                      torch.rand(1, 3, 32, 32),
                      PROJ_DIR / "cifar_models/pruned_directly.onnx")
    attack = torchattacks.PGD(model.cuda(), steps=20, eps=8 / 255)
    model = AdvModel(model=model,
                     val_atk=attack
                     )

    trainer = pl.Trainer(
        gpus=1
    )

    # trainer.test(model, dataloaders=get_loaders("cifar")[-1])
