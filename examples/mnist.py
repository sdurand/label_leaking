import timm
import atks_pckg.torchattacks as torchattacks

from backbones import FNN, CNN
from data_utils import get_loaders
from trainer import train_adv


def run_exp_1(name="mnist_fnn"):
    """
    With a basic FNN:
    1) train a vanilla network
    2) train a PGD network
    3) train a network with transferred PGD on random net
    4) train a network with transferred PGD on a vanilla trained net.
    """
    loaders = get_loaders("mnist", batch_size=100)
    model_1 = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()
    atk = torchattacks.PGD(model=model_1.cuda(), steps=20, eps=0.2)

    trainer, pl_model = train_adv(model=model_1,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="1_vanilla",
                                  attack=atk,
                                  clean=True,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=5,
                                  )

    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 1 done")
    print("=" * 60)
    model_2 = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_2.cuda(), steps=20, eps=0.2)
    trainer, pl_model = train_adv(model=model_2,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="2_PGD",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=5,
                                  gpus=1
                                  )

    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 2 done")
    print("=" * 60)

    model_3 = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    random_model = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    atk = torchattacks.PGD(model=random_model.cuda(), steps=20, eps=0.2)
    trainer, pl_model = train_adv(model=model_3,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="3_pgd_on_random",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=5,
                                  )

    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 3 done")
    print("=" * 60)
    model_4 = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()
    atk = torchattacks.PGD(model=model_1.cuda(), steps=20, eps=0.2)
    trainer, pl_model = train_adv(model=model_4,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="4_pgd_on_trained",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=5,
                                  )

    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 4 done")
    print("=" * 60)


def run_exp_2(name="diff_models"):
    """
    With a basic FNN and a CNN:
    1) train a vanilla CNN network
    2) train a PGD CNN network
    3) train a FNN with transferred PGD on random CNN
    4) train a CNN with transferred PGD on random FNN
    5) train a FNN with transferred PGD on random resnet18
    """

    loaders = get_loaders("mnist", batch_size=100)
    model_1 = CNN(
        input_shape=(28, 28, 1),
        conv_params=[(16, 4, 2, 1), (32, 4, 2, 1)],
        classif_params=[100, 10],
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_1.cuda(), steps=20, eps=0.2)

    trainer, pl_model = train_adv(model=model_1,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="1_vanilla_CNN",
                                  attack=atk,
                                  clean=True,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=5,
                                  )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 1 done")
    print("=" * 60)

    model_1 = CNN(
        input_shape=(28, 28, 1),
        conv_params=[(16, 4, 2, 1), (32, 4, 2, 1)],
        classif_params=[100, 10],
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_1.cuda(), steps=20, eps=0.2)

    trainer, pl_model = train_adv(model=model_1,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="2_PGD_CNN",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=10,
                                  gpus=1
                                  )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 2 done")
    print("=" * 60)

    model_3 = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    random_model = CNN(
        input_shape=(28, 28, 1),
        conv_params=[(16, 4, 2, 1), (32, 4, 2, 1)],
        classif_params=[100, 10],
        activation="relu"
    )

    atk = torchattacks.PGD(model=random_model.cuda(), steps=20, eps=0.2)
    trainer, pl_model = train_adv(model=model_3,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="3_train_FNN_attack_CNN",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=15,
                                  )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 3 done")
    print("=" * 60)
    model_4 = CNN(
        input_shape=(28, 28, 1),
        conv_params=[(16, 4, 2, 1), (32, 4, 2, 1)],
        classif_params=[100, 10],
        activation="relu"
    )

    random_model = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()
    atk = torchattacks.PGD(model=random_model.cuda(), steps=20, eps=0.2)
    trainer, pl_model = train_adv(model=model_4,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="4_train_CNN_attack_FNN",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=5,
                                  )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 4 done")
    print("=" * 60)

    model_4 = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    random_model = timm.create_model("resnet18", pretrained=True, in_chans=1)

    atk = torchattacks.PGD(model=random_model.cuda(), steps=20, eps=0.2)
    trainer, pl_model = train_adv(model=model_4,
                                  loaders=loaders,
                                  name=name,
                                  sub_dir="5_train_FNN_attack_resnet18",
                                  attack=atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=25,
                                  )

    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 5 done")
    print("=" * 60)


def run_exp_3():
    """
    1) FNN train on random FNN attack, but evaluate on attack on itself
    """
    loaders = get_loaders("mnist", batch_size=100)

    model = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    random_model = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    atk = torchattacks.PGD(model=random_model.cuda(), steps=20, eps=0.2)
    val_atk = torchattacks.PGD(model=model.cuda(), steps=20, eps=0.2)

    trainer, pl_model = train_adv(model=model,
                                  loaders=loaders,
                                  name="FNN_self_attack",
                                  attack=atk,
                                  val_atk=val_atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=15,
                                  )
    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 1 done")
    print("=" * 60)


def run_exp_4():
    """
    1) FNN train on random FNN FGSM attack, but evaluate on PGD
    """
    loaders = get_loaders("mnist", batch_size=100)

    model = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    random_model = FNN(
        input_shape=(28, 28),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    ).cuda()

    atk = torchattacks.FGSM(model=random_model.cuda(), eps=0.2)
    val_atk = torchattacks.PGD(model=model.cuda(), steps=20, eps=0.2)

    trainer, pl_model = train_adv(model=model,
                                  loaders=loaders,
                                  name="FNN_FGSM_train_PGD_eval",
                                  attack=atk,
                                  val_atk=val_atk,
                                  clean=False,
                                  early_stopping=False,
                                  weights=(0, 1),
                                  max_epochs=8,
                                  )

    trainer.fit(pl_model)
    trainer.test(pl_model)

    print("=" * 60)
    print("Exp 1 done")
    print("=" * 60)


if __name__ == "__main__":
    run_exp_1()
    run_exp_2()
    run_exp_3()
    run_exp_4()
