import numpy as np
import timm
import atks_pckg.torchattacks as torchattacks
from pytorch_lightning import Trainer

from backbones import FNN, CNN
from data_utils import get_loaders
from trainer import train_adv


def run_cifar_fnn(name="cifar_fnn", gpus=1):
    """
    With a basic FNN:
    1) train a vanilla network
    2) train a PGD network
    3) train a network with transferred PGD on random net
    4) train a network with transferred PGD on a vanilla trained net.
    5) train a network with transferred PGD on PGD trained
    """
    loaders = get_loaders("cifar", batch_size=100)
    model_vanilla = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_vanilla, steps=20, eps=8 / 255)

    trainer, pl_model = train_adv(
        model=model_vanilla,
        loaders=loaders,
        name=name,
        sub_dir="1_vanilla",
        val_atk=atk,
        clean=True,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=12,
        gpus=gpus
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 1 done")
    print("=" * 60)

    model_PGD = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_PGD, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=model_PGD,
        loaders=loaders,
        name=name,
        sub_dir="2_PGD",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=12,
        gpus=gpus
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 2 done")
    print("=" * 60)

    fnn_on_random = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    random_model = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    atk = torchattacks.PGD(model=random_model, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=fnn_on_random,
        loaders=loaders,
        name=name,
        sub_dir="3_pgd_on_random",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=12,
        gpus=gpus
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 3 done")
    print("=" * 60)
    fnn_on_vanilla = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_vanilla, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=fnn_on_vanilla,
        loaders=loaders,
        name=name,
        sub_dir="4_pgd_on_vanilla",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=12,
        gpus=gpus
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 4 done")
    print("=" * 60)

    fnn_on_pgd = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )
    atk = torchattacks.PGD(model=model_PGD, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=fnn_on_pgd,
        loaders=loaders,
        name=name,
        sub_dir="5_pgd_on_pgd",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=12,
        gpus=gpus
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)
    print("=" * 60)
    print("Exp 5 done")
    print("=" * 60)


def run_exp_resnet(name="cifar_resnet"):
    """
    1) train a vanilla resnet
    2) train a PGD resnet
    3) train a FNN with transferred PGD on random resnet
    4) train a FNN with transferred PGD on a vanilla trained resnet.
    5) train a FNN with transferred PGD on PGD trained resnet.
    6) train a resnet with transferred PGD on random FNN. (converse of 3)
    """
    loaders = get_loaders("cifar", batch_size=256)
    model_vanilla = timm.create_model("resnet18", pretrained=False)
    atk = torchattacks.PGD(model=model_vanilla, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=model_vanilla,
        loaders=loaders,
        name=name,
        sub_dir="1_vanilla",
        attack="PGD",
        val_atk=atk,
        clean=True,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=50,
        lr=0.1,
        gpus=2
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)

    model_PGD = timm.create_model("resnet18", pretrained=False)
    atk = torchattacks.PGD(model=model_PGD, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=model_PGD,
        loaders=loaders,
        name=name,
        sub_dir="2_PGD",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=200,
        gpus=2
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)

    model_fnn = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    random_model = timm.create_model("resnet18", pretrained=False)

    atk = torchattacks.PGD(model=random_model, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=model_fnn,
        loaders=loaders,
        name=name,
        sub_dir="3_fnn_on_random_resnet_PGD",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=200,
        gpus=2
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)

    model_fnn = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    atk = torchattacks.PGD(model=model_vanilla, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=model_fnn,
        loaders=loaders,
        name=name,
        sub_dir="4_fnn_on_vanilla_resnet_PGD",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=300,
        gpus=2
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)

    model_fnn = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    atk = torchattacks.PGD(model=model_PGD, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=model_fnn,
        loaders=loaders,
        name=name,
        sub_dir="5_fnn_on_PGD_resnet_PGD",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=300,
        gpus=2
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)

    model_fnn = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )

    resnet = timm.create_model("resnet18", pretrained=False)
    atk = torchattacks.PGD(model=model_fnn, steps=20, eps=8 / 255)
    trainer, pl_model = train_adv(
        model=resnet,
        loaders=loaders,
        name=name,
        sub_dir="6_resnet_on_PGD_random_fnn",
        attack=atk,
        clean=False,
        early_stopping=False,
        weights=(0, 1),
        max_epochs=50,
        gpus=2
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)


if __name__ == "__main__":
    run_cifar_fnn("cifar_fnn")
