import sys

import timm

import atks_pckg.torchattacks as torchattacks
from backbones import FNN
from data_utils import get_loaders
from trainer import train_adv

if __name__ == "__main__":
    if len(sys.argv) > 1:
        batch_size = int(sys.argv[1])
    else:
        batch_size = 100
    lr = 0.001 * (batch_size // 32)
    loaders = get_loaders("cifar", batch_size=batch_size)

    model_PGD = FNN(
        input_shape=(32, 32, 3),
        layer_sizes=(256, 256, 256, 256, 10),
        activation="relu"
    )
    PGD = torchattacks.PGD
    model_vanilla = timm.create_model("resnet18", pretrained=True)
    atk = PGD(model=model_vanilla, steps=20, eps=8 / 255)

    trainer, pl_model = train_adv(
        gpus=1,
        model=model_PGD,
        attack=PGD,
        attack_kwargs={
            "eps": 8 / 255,
            "steps": 20
        },
        atk_model=model_vanilla,
        loaders=loaders,
        name="test_factoryia_mono_gpu_PGD",
        sub_dir="1_vanilla",
        clean=False,
        early_stopping=True,
        monitor="clean_val_acc",
        es_mode="max",
        weights=(0, 1),
        max_epochs=3,
        lr=lr
    )
    trainer.fit(pl_model)
    trainer.test(pl_model)
